import { Outlet, Link } from "react-router-dom";

const Layout = () => {
  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to="/compra">compra</Link>
          </li>
          <li>
            <Link to="/producto">producto</Link>
          </li>
          <li>
            <Link to="/crear-producto">crear producto</Link>
          </li>
        </ul>
      </nav>

      <Outlet />
    </>
  )
};

export default Layout;