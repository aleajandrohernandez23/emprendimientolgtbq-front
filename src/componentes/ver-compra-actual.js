import axios from "axios";
import { useState } from "react";

function VerCompraActual() {
    const [compra, setCompra] = useState(null)


    const cargarCompraActual = (id) => {
        axios.get(`http://localhost:4000/compra/${id}`).then(res => {
            console.log(res.data);
            setCompra(res.data);
        }).catch(err => {
            console.log(err);
        })
    }

    function handleSubmit(e) {
        e.preventDefault();

        const formData = new FormData(e.target)
        const form = Object.fromEntries(formData.entries())


        cargarCompraActual(form.idCliente);


    }



    function handleClick() {
        console.log('Buscando los productos');
        cargarCompraActual()
    }
    return (<div>
        <button onClick={handleClick}>Compra Actual</button>
        <form onSubmit={handleSubmit}>
            <input name="idCliente" />
            <button type="submit">Buscar</button>
        </form>

        <div>
            {

                compra != null ?
                    <div>
 
                        <ul>
                            {
                                compra.producto_Agregado.map(pa => <li>{pa.producto.nombre}</li>)
                            }
                        </ul>
                        <h2><b>Total:</b>{compra.totalCompra}</h2>
                        <h2><b>Total Items:</b>{compra.totalItems}</h2>

                    </div> :
                    <label>No hay una compra cargada</label>
            }
        </div>
    </div>)
}

export default VerCompraActual;