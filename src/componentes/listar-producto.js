import React from "react";
import axios from "axios";
import { useEffect,useState } from "react";

function ListarProductos() {
    const [productos, setProductos] = useState([])
    const productosActualizados = 0;
    const cargarProductos = ()=>{
        axios.get('http://localhost:4000/producto').then(res => {
            console.log(res.data);
            setProductos(res.data);
        }).catch(err => {
            console.log(err);
        })
    }

    useEffect(()=>{
        cargarProductos()
    }, [productosActualizados]) 

    function handleClick() {
        console.log("Buscando los productos");
        cargarProductos()
    }
    return (<div>
        <button onClick={handleClick}>Cargar Productos</button>
        <ul>    
            {
                productos.map(producto => <li>{producto.nombre}</li>)
            }
            
        </ul>
    </div>)
}

export default ListarProductos;