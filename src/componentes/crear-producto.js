import React from "react";
import axios from "axios";

function CrearProducto() {
    function handleSubmit(e) {
        e.preventDefault();

        const formData = new FormData(e.target)
        const form = Object.fromEntries(formData.entries())

        axios.post('http://localhost:4000/producto', form).then(res => {
            console.log(res);
        }).catch(err => {
            console.error(err)
        })
    }

    return (
        <div className="crear-producto">
            <h1>Producto</h1>
            <form onSubmit={handleSubmit}>
                <div className="form-control">
                    <label>Nombre:</label><input name="nombre"></input>
                </div>
                <div className="form-control">
                    <label>valor:</label><input name="valor"></input>
                </div>
                <div className="form-control">
                    <label>categoria:</label><input name="categoria"></input>
                </div>
                <div className="form-control">
                    <label>vendedor:</label><input name="vendedorId"></input>
                </div>
                <button type="submit">Crear</button>
            </form>
        </div>
    );
}

export default CrearProducto;
