import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import CrearProducto from './componentes/crear-producto';
import ListarProductos from './componentes/listar-producto';
import VerCompraActual from './componentes/ver-compra-actual';
import Layout from './componentes/layaout';




function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="compra" element={<VerCompraActual />} />
            <Route path="producto" element={<ListarProductos />} />
            <Route path="crear-producto" element={<CrearProducto />} />
          </Route>
        </Routes>
      </BrowserRouter>


    </div>

  );
}


export default App;

